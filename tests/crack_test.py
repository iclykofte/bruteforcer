import unittest

from bruteforcer.attack_thread import AttackThread
from bruteforcer.config import Config
from bruteforcer.dialect_factory import DialectFactory
from bruteforcer.password_generator import PasswordGenerator


class BruteForcerTest(unittest.TestCase):
    def test_find_password(self):
        filename = 'encrypted/linux-commands.png2.7z'
        df = DialectFactory(filename)
        dialect = df.get_dialect()
        pg = PasswordGenerator()

        attack_threads = []
        for i in range(Config.THREAD_COUNT):
            th = AttackThread(dialect=dialect, password_generator=pg)
            attack_threads.append(th)
            th.start()

        # Wait for all threads to complete
        for th in attack_threads:
            th.join()

        self.assertEqual(AttackThread.PASSWORD, 'I#$')

    def test_try_password(self):
        filename = 'encrypted/linux-commands.png.7z'
        df = DialectFactory(filename)
        dialect = df.get_dialect()
        pg = PasswordGenerator()

        self.assertEqual(dialect.try_password('Ab'), True)

    def test_get_dialect_name(self):
        filename = 'encrypted/linux-commands.png.7z'
        df = DialectFactory(filename)
        dialect = df.get_dialect()

        print(dialect.get_name())

        self.assertEqual(dialect.get_name(), '7z')

    def test_get_next(self):
        pg = PasswordGenerator()

        counter = -1
        while True:
            counter += 1
            val = pg.next()
            if counter == 16:
                self.assertEqual(val, 'II')
                break

