# BruteForcer

Simple extensible brute force password attack tool for password protected executables. Program initially was written for 7zip files, but can be easily
extended to other executables.

## Technology:
Python 3.x

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

I developed on virtualenvironment. If you want to add support for other formats add command line arguments to dialects.
You're supposed to inherit from dialect class. And be sure to check file format at format_checker.
If the file IS really has the signature of the desired format then implement required factory initialization
 at dialect_factory class.

## Running the tests

Tests for the important functionality are added.

## Deployment

You can install it with the command:

sudo python setup.py install


## Built With

python 3.x

## Contributing

Please feel free to contact me.

## Versioning

I use [git](https://git-scm.com/) for versioning.  

## Authors

* **Hayati Gonultas** - *Initial work* - [iclykofte](https://bitbucket.org/iclykofte/)

## License

This project is licensed under the MIT License.