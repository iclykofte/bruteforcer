import threading

class AttackThread(threading.Thread):
    FOUND_PASSWORD = False
    PASSWORD = ''

    def __init__(self, dialect, password_generator):
        threading.Thread.__init__(self)
        self.dialect = dialect
        self.password_generator = password_generator

    def run(self):
        while True:
            if AttackThread.FOUND_PASSWORD == True:
                break
            pw = self.password_generator.next()
            rv = self.dialect.try_password(pw)
            if rv:
                AttackThread.FOUND_PASSWORD = True
                AttackThread.PASSWORD = pw
                break

