class Dialect():
    def __init__(self):
        self.cmd = ''
        self.extract = ''
        self.password = ''

    def get_name(self):
        return self.cmd

    def try_password(self, password):
        raise NotImplementedError('You\'re supposed to inherit this base class')
        return False