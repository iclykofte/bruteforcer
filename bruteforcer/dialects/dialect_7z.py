import subprocess
from bruteforcer.dialects.dialect import Dialect
import logging
from bruteforcer.config import Config


class Dialect7z(Dialect):
    def __init__(self, filename):
        Dialect.__init__(self)
        self.cmd = '7z'
        self.extract = 'x'
        self.password = '-p'
        self.filename = filename

    def try_password(self, password):
        logging.info('trying password: %s' % password)
        try:
            # FIXME
            # no spaces in filenames!!
            cmd = "%s -y %s %s%s %s" % (self.cmd, self.extract, self.password, password, self.filename)
            args = cmd.split(' ')
            proc = subprocess.Popen(args, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
            proc.wait()

            if proc.returncode == 0:
                logging.info('password found: %s', password)
                return True
            else:
                logging.debug('incorrect password: %s, exit code: %d' % (password, proc.returncode))

        except OSError as e:
            logging.error(e.strerror)

        return False
