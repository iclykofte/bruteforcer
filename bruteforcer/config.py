class Config(object):
    # bigger alphabet
    #ALPHABET   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}|'

    # smaller alphabet for test purposes
    ALPHABET   = 'IKLOTiklot!@#$*+'

    MAX_LENGTH = 20
    PATTERN    = '*'
    DIALECT_STDERR = None
    THREAD_COUNT = 10




