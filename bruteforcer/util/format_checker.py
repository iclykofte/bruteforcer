import subprocess
import logging


class FormatChecker():
    def __init__(self, filename=None):
        self.filename = filename

    def is7z(self):
        try:
            cmd = 'file %s' % self.filename
            args = cmd.split(' ')
            with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
                desc = str(proc.stdout.read())
                if '7-zip' in desc:
                    return True
                else:
                    raise NotImplementedError('File format %s not implemented!' % desc)
        except OSError as e:
            logging.error(e.strerror)

        return False