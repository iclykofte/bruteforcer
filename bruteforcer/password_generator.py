from bruteforcer.config import Config
import logging
import threading

threadLock = threading.Lock()

class PasswordGenerator():
    def __init__(self):
        self.last_item = 0
        self.alphabet_len = len(Config.ALPHABET)

    def __iter__(self):
        return self

    def int2base(self, x, base):
        if x == 0:
            return Config.ALPHABET[0]

        digits = []

        while x:
            x -= 1
            digits.append(Config.ALPHABET[x % base])
            x //= base # make sure it IS a integer division

        digits.reverse()

        result = ''.join(digits)

        if len(result) > Config.MAX_LENGTH:
            logging.warning('All combinations exhausted!')
            exit(0)

        return result

    def next(self):
        threadLock.acquire()
        self.last_item += 1
        curr_val = self.last_item
        threadLock.release()

        logging.debug(str(curr_val))
        return self.int2base(int(curr_val), self.alphabet_len)

