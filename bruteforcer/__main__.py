import sys

from bruteforcer.config import Config
from bruteforcer.dialect_factory import DialectFactory
from bruteforcer.password_generator import PasswordGenerator
from bruteforcer.attack_thread import AttackThread

def main(args=None):
    filename = args[0]
    df = DialectFactory(filename)
    dialect = df.get_dialect()
    pg = PasswordGenerator()

    attack_threads = []
    for i in range(Config.THREAD_COUNT):
        th = AttackThread(dialect=dialect, password_generator=pg)
        attack_threads.append(th)
        th.start()

    # Wait for all threads to complete
    for th in attack_threads:
        th.join()

    print('Cracked password: %s' % AttackThread.PASSWORD)




if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print('Specify filename to try brute force attack!')
        exit(1)

    main(sys.argv[1:])