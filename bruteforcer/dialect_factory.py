from bruteforcer.util.format_checker import FormatChecker
from bruteforcer.dialects.dialect_7z import Dialect7z
import logging

class DialectFactory():
    def __init__(self, filename):
        self.filename = filename

    def get_dialect(self):
        cfc = FormatChecker(self.filename)

        try:
            if cfc.is7z():
                return Dialect7z(self.filename)
            else:
                logging.error('Not supported zip format!')
        except NotImplementedError as e:
            logging.error('Not supported zip format: %s' % e.__str__())