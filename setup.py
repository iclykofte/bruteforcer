from setuptools import setup, find_packages

setup(
    name='BruteForcer',
    version='0.1',
    url='https://bitbucket.org/iclykofte/bruteforcer',
    license='MIT',
    author='Hayati Gonultas',
    author_email='hayati.gonultas@gmail.com',
    description='Simple extensible brute force application',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=[
    ],
    entry_points='''
        [console_scripts]
        bruteforcer=bruteforcer.__main__:main
    ''',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Desktop Environment',
        'Intended Audience :: Developers, Users',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)